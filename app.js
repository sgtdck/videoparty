var express = require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var io = require('socket.io')(http);
var youtube = require('youtube-search');
var fs = require('fs');

app.set('models', __dirname + '/models');

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.set('controllers', __dirname + '/controllers');

app.use(express.static(path.join(__dirname, 'public')));

if(process.env.NODE_ENV == "development") {
	console.log('Started app in development mode');
    app.use(express.static(path.join(__dirname, 'models')));
}

// dynamically include routes (Controller)
fs.readdirSync(app.get('controllers')).forEach(function (file) {
  if(file.substr(-3) == '.js') {
      route = require(app.get('controllers') + '/' + file);
      route.controller(app, io);
  }
});

var port = process.env.PORT || 3000;
http.listen(port, function(){
  console.log('listening on *:'+port);
});

var opts = {
  maxResults: 10,
  startIndex: 1
};

