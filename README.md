# Video party!

## Dev setup

Assumed that you have Node.js and thus NPM installed.

1. `npm install -g bower`
2. `npm install -g grunt-cli`
3. `npm install`
4. set the environment variable `NODE_ENV` to `development`
5. `node app.js`
