// var Playlist = function(options) {
function Playlist(items) {
    var self = this;

    this.setItems(items);
}

Playlist.prototype.addSong = function(item) {
    // if(!(item instanceof PlayItem)) {
    //     console.error('Item is not an instance of PlayItem');
    //     return this;
    // }

    if(this.exists(item)) {
        console.error('Item already exists in playlist');
        return this;
    }

    this.items.push(item);

    return this;
}

Playlist.prototype.songExists = function(item) {
    for(var i = 0; i < this.items.length; ++i) {
      if(this.items[i] === item) {
        return true;
      }
    }

    return false;
}

Playlist.prototype.getSong = function(id) {
    return this.items[id];
}
Playlist.prototype.removeSong = function(party) {
    var idx =  this.parties.indexOf(party);

    if(idx > -1) {
        this.parties.splice(idx, 1);
    }
}
Playlist.prototype.setItems = function(items) {
    this.items = [];

    if(!(items instanceof Array)) {
        return this;
    }

    for(var i in items) {
        this.add(items[i]);
    }

    return this;
}

if(typeof module === 'object') {
    module.exports = Playlist;
}
