function Clientlist() {
    var self = this;

	this.clients = {};
}

Clientlist.prototype.addClient = function(client) {
	this.clients[client.getId()] = client;
}
Clientlist.prototype.removeClient = function(client) {
	delete this.clients[client];
}
Clientlist.prototype.getClient = function(id) {
	return this.clients[id];
}
Clientlist.prototype.hasClient = function(id) {
	return (id in this.clients);
}

if(typeof module === 'object') {
    module.exports = Clientlist;
}