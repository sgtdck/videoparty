function Partylist() {
    var self = this;

	this.parties = {};
}

Partylist.prototype.addParty = function(party) {
	this.parties[party.getCode()] = party;
}
Partylist.prototype.removeParty = function(party) {
	var idx =  this.parties.indexOf(party);

	if(idx > -1) {
		this.parties.splice(idx, 1);
	}
}
Partylist.prototype.getParty = function(code) {
	return this.parties[code];
}
Partylist.prototype.hasParty = function(code) {
	return (code in this.parties);
}
// Partylist.prototype.addPartyIfNotExists = function(code) {
// 	if(!this.partyExists(code)) {
// 		var party = new Party(code);

// 		this.addParty(party);

// 		return party;
// 	}

// 	return this.getParty(code);
// }

if(typeof module === 'object') {
    module.exports = Partylist;
}