function Controls($element) {

    this.$element = $element;

    this.$play = $element.find('#play');
    this.$pause = $element.find('#pause');

    this.clear = function() {
        this.$element.find('.control').removeClass('active');
    }
    this.play = function() {
        this.clear();
        this.$play.addClass('active');
    };
    this.pause = function() {
        this.clear();
        this.$pause.addClass('active');
    };

    this.getPlay = function() {
        return this.$play;
    }

    this.getPause = function() {
        return this.$pause;
    }
}

// Youtube.prototype = Object.create(Player.prototype);
// Youtube.prototype.constructor = Youtube;

if(typeof module === 'object') {
    module.exports = Controls;
}
