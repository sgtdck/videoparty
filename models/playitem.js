if(typeof document === 'undefined') {
    var Player = require(__dirname + '/player.js');
}

function PlayItem(id, title, player) {
    var self = this;

    this.id = id;
    this.title = title;
    this.votes = 1;
    this.setPlayer(player);
}

PlayItem.prototype.getId = function() {
    return this.id;
}
PlayItem.prototype.getTitle = function() {
    return this.title;
}

PlayItem.prototype.play = function() {
    this.player.play(id);
}
PlayItem.prototype.pause = function() {
    this.player.pause(id);
}
PlayItem.prototype.restart = function() {
    this.player.restart(id);
}

PlayItem.prototype.getPlayer = function() {
    return this.player;
}
PlayItem.prototype.setPlayer = function(player) {
    // if(!(player instanceof Player)) {
    //     console.log('This is not a Player object');
    //     return this;
    // }

    this.player = player;

    return this;
}
PlayItem.prototype.getVotes = function() {
    return this.votes;
}
PlayItem.prototype.setVotes = function(votes) {
    // NOTE: this is deprecated; this is here to mimic votes until this is implemented properly.
    this.votes = votes;
}
PlayItem.prototype.addVote = function() {
    return ++this.votes;
}

if(typeof module === 'object') {
    module.exports = PlayItem;
}
