function Party(code) {
    var self = this;

    this.code = code;
    this.playItems = {};
    this.clients = [];
}
Party.prototype.getCode = function() {
	return this.code;
}
Party.prototype.addSong = function(playItem) {
	this.playItems[playItem.getId()] = playItem;

	// hacky way of sorting the songs. abusing 'votes' :)
	var numSongs = Object.keys(this.playItems).length;
	playItem.setVotes(numSongs + 1);

}
Party.prototype.removeSong = function(id) {
	// var idx =  this.playItems.indexOf(playItem);

	// if(idx > -1) {
	// 	this.playItems.splice(idx, 1);
	// }
	console.log("Removed video " + id);
	delete this.playItems[id];
}
Party.prototype.replaceSongList = function(playItemHashSet) {
	if(!(playItemHashSet instanceof Object)) {
		console.log('Could not replace song list; not an object');
		return this.playItems;
	}

	this.playItems = {}; // empty the list

	for(var i in playItemHashSet) {
		this.addSong(playItemHashSet[i]);
	}

	// this.playItems = playItemHashSet;

	return this.playItems;
}
Party.prototype.getSong = function(code) {
	return this.playItems[code];
}
Party.prototype.songExists = function(code) {
	
}
Party.prototype.getAllSongs = function() {
	return this.playItems;
}
Party.prototype.getNextSong = function() {
	var numSongs = Object.keys(this.playItems).length;
	var songArray = new Array;

	for(var i in this.playItems) {
		songArray.push(this.playItems[i]);
	}

	songArray.sort(function(a,b) {
		if(a.getVotes() < b.getVotes()) return -1;
		if(b.getVotes() > a.getVotes()) return 1;
		return 0;
	});

    if(numSongs < 1) {
      console.log("Song list empty");
      return;
    }

    return songArray.shift(); // fetch first song; i.e. least votes. until we actually set up votes correctly.
}
Party.prototype.addClient = function(client) {
	this.clients.push(client);
}
Party.prototype.removeClient = function(client) {
	var idx = this.clients.indexOf(client);

	if(idx > -1) {
		this.clients.splice(idx, 1);
	}
}
Party.prototype.getClient = function(id) {
	return this.clients[id];
}
Party.prototype.getAllClients = function() {
	return this.clients;
}

if(typeof module === 'object') {
    module.exports = Party;
}