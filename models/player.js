function Player() {
    this.play = function(id) {
        throw new Error("Not implemented, interface only");
    }
    this.pause = function() {
        throw new Error("Not implemented, interface only");
    }
}

function Youtube($player, options, controls) {
    this.element = $player;
    this.options = options;
    this.controls = controls;
    this.isPaused = false;

    this.play = function(id) {
        if(id !== null) {
            this.options.video = id;
            this.element.player(this.options);
            this.init();
        }
        if(this.player) {
            this.player.playVideo();
            this.controls.play();
            this.isPaused = false;
        }
    };
    this.pause = function() {
        if(this.player) {
            if(this.isPaused) {
                this.player.playVideo();
                this.controls.play();
            }
            else {
                this.player.pauseVideo();
                this.controls.pause();
            }
            this.isPaused = !this.isPaused;
        }
    };
    this.setPaused = function(isPaused) {
        this.isPaused = isPaused;
    }
    this.getCurrentSong = function() {
        return this.player.current_video;
    }
    this.init = function() {
        if(this.element.data('player') && !this.player) {
            this.player = this.element.data('player').p;
        }
    }
};
Youtube.prototype = Object.create(Player.prototype);

// function Youtube($player, options, controls) {
//     this.element = $player;
//     this.options = options;
//     this.controls = controls;
//     this.isPaused = false;

//     this.play = function(id) {
//         if(id !== null) {
//             this.options.video = id;
//             this.element.player(this.options);
//             this.init();
//         }
//         console.log('player obj: ', this.player);
//         if(this.player) {
//             console.log(this.player.current_video);
//             this.player.playVideo();
//             this.controls.play();
//             this.isPaused = false;
//         }
//     };
//     this.pause = function() {
//         if(this.player) {
//             if(this.isPaused) {
//                 this.player.playVideo();
//                 this.controls.play();
//             }
//             else {
//                 this.player.pauseVideo();
//                 this.controls.pause();
//             }
//             this.isPaused = !this.isPaused;
//         }
//     };
//     this.setPaused = function(isPaused) {
//         this.isPaused = isPaused;
//     }
//     this.init = function() {
//         if(this.element.data('player') && !this.player) {
//             this.player = this.element.data('player').p;
//         }
//     }
// }

// Youtube.prototype = Object.create(Player.prototype);
// Youtube.prototype.constructor = Youtube;

if(typeof module === 'object') {
    module.exports.Player = Player;
    module.exports.Youtube = Youtube;
}
