function Client(id, name, party) {
    var self = this;

    this.id = id;
    this.name = name;
    this.party = party;
}

Client.prototype.getId = function() {
    return this.id;
}
Client.prototype.getName = function() {
    return this.name;
}
Client.prototype.getParty = function() {
	return this.party;
}

if(typeof module === 'object') {
    module.exports = Client;
}
