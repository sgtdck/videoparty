module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    },
    bower: {
      dev: {
        dest: 'public/vendor/',
        options: {
          expand: true,
          packageSpecific: {
            'socket.io-client': {
              files: [
                "socket.io.js",
                "lib/*"
              ]
            },
            'jquery': {
                files: [
                  'dist/jquery.min.js'
                ]
            },
            'jquery.tube': {
              files: [
                'jquery.tube.min.js'
              ]
            },
            'bootstrap': {
              files: [
                'dist/css/*',
                'dist/fonts/*',
                'dist/js/*'
              ]
            }
          }
        }
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  // grunt.loadNpmTasks('grunt-contrib-uglify');

  // grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.loadNpmTasks('grunt-bower');

  // Default task(s).
  grunt.registerTask('default', ['bower']);

};
