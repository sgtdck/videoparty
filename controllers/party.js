module.exports.controller = function(app, io) {
  var bodyParser = require('body-parser');
  var validator = require('validator');
  var Player = require(app.get('models') + '/player');
  var Youtube = Player.Youtube;
  var Playlist = require(app.get('models') + '/playlist');
  var PlayItem = require(app.get('models') + '/playitem');
  var Party = require(app.get('models') + '/party');
  var Partylist = require(app.get('models') + '/partylist');
  var Clientlist = require(app.get('models') + '/clientlist');
  var Client = require(app.get('models') + '/client');


  var nsp = io.of('/party');

  var partylist = new Partylist();
  var clientlist = new Clientlist();
  var youtube = new Youtube();

  nsp.on('connection', function(socket){
    console.log('Client connected');

    socket.on('join', function(code) {
      if(!validator.isNumeric(code)) {
        return;
      }

      code = validator.toString(code);

      var party = partylist.getParty(code);
      var client = new Client(socket.id, 'Anonymous', party);

      clientlist.addClient(client);
      party.addClient(client);

      socket.join(code);
      console.log(client.getName() + ' joined party ' + code);
      socket.to(code).emit('message', client.getName() + ' joined this party!');

      if(party.getAllClients().length === 1) {
        console.log(client.getName() + ' is set to \'primary\' for party ' + code);
        nsp.connected[socket.id].emit('become-primary', 'You are now the primary device.');
      }
    });

    socket.on('hi', function(msg) {
      if(!clientlist.hasClient(socket.id)) {
        return;
      }
      
      var client = clientlist.getClient(socket.id);
      console.log('Message from ' + client.getName() + ': ' + msg);
    });

    socket.on('add', function(msg){
      if(!clientlist.hasClient(socket.id)) {
        return;
      }

      var client = clientlist.getClient(socket.id);
      console.log(client.getName() + ' added a video: ' + msg)
    });

    socket.on('disconnect', function(){
      if(!clientlist.hasClient(socket.id)) {
        return;
      }

      var client = clientlist.getClient(socket.id);

      console.log(client.getName() + ' disconnected');

      client.getParty().removeClient(client);
      clientlist.removeClient(client);
    });

    socket.on('want-primary', function(code) {
      if(!validator.isNumeric(code)) {
        return;
      }

      if(!clientlist.hasClient(socket.id)) {
        return;
      }

      var client = clientlist.getClient(socket.id);
      console.log(client.getName() + ' wants to become primary for ' + code);

      code = validator.toString(code);

      nsp.to(code).emit('change-primary', '');
    });
  });

  app.use( bodyParser.urlencoded({extended: true})); // to support URL-encoded bodies

  // Create party
  app.post('/party/create', function(req, res) {
    // res.send(req);

    var party = Math.round(1000 * Math.random());


    console.log('created a party ' + party);


    res.redirect('/party/' + party);
  });

  // Join a party
  app.get('/party/:code', function(req, res) {
    if(!validator.isNumeric(req.params.code)) {
      res.status(404).send('Party not found');
      return;
    }

    var code = validator.toString(req.params.code);

    if(!partylist.hasParty(code)) {
      var party = new Party(code);
      partylist.addParty(party);  
    }

    var party = partylist.getParty(code);

    res.render('party', { party: code, partylist: party.getAllSongs(), environment: process.env.NODE_ENV});
  });

  // URI:     /party/:code/add
  // Method:  POST
  // Form:    id (int)
  app.post('/party/:code/add', function(req, res) {
    if(!validator.isNumeric(req.params.code)) {
      res.status(404).send('Party not found');
      return;
    }

    var code = validator.toString(req.params.code);

    if(!partylist.hasParty(code)) {
      var party = new Party(code);
      partylist.addParty(party);
    }

    var party = partylist.getParty(code);
    var songs = party.getAllSongs();
    var numSongs = Object.keys(songs).length;

    if(numSongs > 100) {
      res.json({title: "TOO MANY SONGS IN THE LIST"});
      return;
    }

    for(var i in songs) {
      if(songs[i].getId() === req.body.id) {
        res.json({title: "ALREADY EXISTS"});
        return;
      }
    }

    var song = new PlayItem(req.body.id, req.body.title, youtube);
    party.addSong(song);
    // partylist[party].push(req.body);

    nsp.to(code).emit('refresh', '');

    // auto-play if it's the first song
    console.log(numSongs + ' songs in this party');
    if(numSongs === 0) {
      console.log('Auto-playing ' + req.body.id + ' at party ' + code);
      nsp.to(code).emit('play', req.body.id);
    }

    // console.log('joined party ' + party);

    res.json({title: "YOLO"});
  });

  app.delete('/party/:code/delete/:id', function(req, res) {
    if(!validator.isNumeric(req.params.code)) {
      res.status(404).send('Party not found');
      return;
    }

    // var party = validator.toString(req.params.code);
    var code = validator.toString(req.params.code);

    if(!partylist.hasParty(code)) {
      var party = new Party(code);
      partylist.addParty(party);
    }

    var party = partylist.getParty(code);
    var songs = party.getAllSongs();
    var id = validator.toString(req.params.id);

    party.removeSong(id);
    res.json({title: "OK"});
    nsp.to(code).emit('refresh', '');

    // res.redirect('/party/' + code + '/next');

    // if(partylist[party].length) {
    //   var newId = partylist[party][Math.floor(Math.random()*partylist[party].length)].id;
    //   console.log('Now going to play ' + newId);
    //   nsp.to(party).emit('play', newId);
    // }
  });

  app.get('/party/:code/next', function(req, res) {
    var code = validator.toString(req.params.code);

    if(!partylist.hasParty(code)) {
      var party = new Party(code);
      partylist.addParty(party);
    }

    var party = partylist.getParty(code);
    var song = party.getNextSong();

    if(typeof song === 'undefined') {
      res.json({error: 'No next song found!'});
    }

    console.log('Now going to play ' + song.getId());
    nsp.to(code).emit('play', song.getId());

    res.json(song);
  });

  app.get('/party/:code/list', function(req, res) {
    if(!validator.isNumeric(req.params.code)) {
      res.status(404).send('Party not found');
      return;
    }

    var code = validator.toString(req.params.code);

    if(!partylist.hasParty(code)) {
      var party = new Party(code);
      partylist.addParty(party);
    }

    var party = partylist.getParty(code);

    res.set('Content-Type', 'application/json');
    res.json(party.getAllSongs());
  });

  app.get('/party/:code/list/:item', function(req, res) {
    if(!validator.isNumeric(req.params.code)) {
      res.status(404).send('Party not found');
      return;
    }

    if(!validator.isNumeric(req.params.item)) {
      res.status(404).send('Item');
      return;
    }

    var item = validator.toString(req.params.item);

    var code = validator.toString(req.params.code);

    if(!partylist.hasParty(code)) {
      var party = new Party(code);
      partylist.addParty(party);
    }

    var party = partylist.getParty(code);

    res.set('Content-Type', 'application/json');

    if(!(item in list[party])) {
      res.json({error: "Item does not exist"});
      return;
    }

    res.json(partylist[party][item]);
  });
};
