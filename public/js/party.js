var PlayItem = require('../../models/playitem');
var Party = require('../../models/party');
// var Playlist = require('../../models/playlist');
var p = require('../../models/player');
var Player = p.Player;
var Youtube = p.Youtube;
var Controls = require('../../models/controls');

var youtube;
$(document).ready(function() {
  var primaryDevice = false;
  var playOnDevice = false;

  var currentId;
  var party = new Party($('body').data('party-id'));
  var socket = io('/party');

  socket.emit('join', party.getCode());
  socket.on('message', function (data) {
    console.log('Message: ' + data);
  });

  var timeout;

  var $tube = $('#tube');
  var $player = $('#player');
  var $list = $('#list');
  var $controls = $('#controls');

  var $checkboxPrimaryDevice = $('#checkbox-primary-device');
  var $checkboxPlayDevice = $('#checkbox-play-device');

  var controls = new Controls($controls);

  var event_handler = function (event) {
    console.log('on ' + event, this);
  };

  var start = function (event) {
    youtube.init();
    youtube.setPaused(false);
    controls.play();

    var song = party.getSong(youtube.getCurrentSong());
    document.title = song.getTitle();
  };

  var pause = function (event) {
    youtube.setPaused(true);
    controls.pause();
  }

  var end = function (event) {
    if(primaryDevice) {
      $.ajax({
        type: "DELETE",
        url: '/party/' + party.getCode() + '/delete/' + currentId
      });
      
      $.ajax({
        type: "GET",
        url: '/party/' + party.getCode() + '/next'
      });
    }
  };

  var togglePrimaryDevice = function(bool) {
    $checkboxPrimaryDevice.prop('disabled', bool);
    $checkboxPrimaryDevice.prop('checked', bool);
    primaryDevice = bool;
  };

  var togglePlayDevice = function(bool) {
    $checkboxPlayDevice.prop('checked', bool);
    playOnDevice = bool;
  };

  var playerConfig = { video: currentId, playerVars: { autoload: true, autoplay: true, controls: 1 }, events: { end: end, play: start, pause: pause } };

  var youtube = new Youtube($player, playerConfig, controls);

  socket.on('play', function(data) {
    if(playOnDevice) {
      currentId = data;
      var song = party.getSong(currentId);

      // playerConfig.video = currentId;
      // $player.player(playerConfig);
      youtube.play(currentId);
    }
  });
  socket.on('refresh', function(data) {
    $.ajax({
      type: "GET",
      url: '/party/' + party.getCode() + '/list',
      success: function(data) {
        $list.empty();

        var playItems = {};
        for(var i in data) {
          var playItem = new PlayItem(data[i].id, data[i].title, youtube);
          playItems[playItem.getId()] = playItem;
        }
        var songList = party.replaceSongList(playItems);
        console.log(songList);

        for(var i in songList) {
          var item = songList[i];
          $list.append('<li class="list-group-item"><span><a href="#" data-video-id="' + item.getId() + '" class="play">' + item.getTitle() + '</a>' /*' <a href="/party/' + party.getCode() + '/vote/up/' + item.getId() + '" class="vote">[vote]</a> </a> <a href="/party/' + party.getCode() + '/vote/remove/' + item.getId() + '" class="vote">[vote remove]</a>'*/ + '</li>');
        }
      }
    })
  });
  socket.on('become-primary', function(data) {
    togglePrimaryDevice(true);
    togglePlayDevice(true);
  });
  socket.on('change-primary', function(data) {
    console.log('OOOH, someone changed the primary device');

    if(primaryDevice && $checkboxPrimaryDevice.data('requested')) {
      togglePrimaryDevice(true);
      togglePlayDevice(true);
      $checkboxPrimaryDevice.data('requested', false);
    }
    else {
      togglePrimaryDevice(false);
    }
  });

  controls.getPlay().click(function(e) {
    youtube.play();
  });

  controls.getPause().click(function(e) {
    youtube.pause();
  });

  $tube.on('change', 'ol', function() {
    console.log("TUBE LOADED");
  });

  $list.on('click', '.play', function(e) {
    var videoId = $(e.currentTarget).data('video-id');
    //- $player.data('player').play(videoId);
    currentId = videoId;
    playerConfig.video = currentId;
    youtube.play(currentId);
    // $player.player(playerConfig);
  });

  $tube.on('click', '.add-item', function(e) {
    e.preventDefault();
    var $target = $(e.currentTarget);
    var videoId = $target.data('video-id');
    var index = $target.data('index');
    var videos = $tube.data('tube').videos;

    if(videos[index]) {
      var video = new PlayItem(videos[index].id, videos[index].title, youtube);
      // var video = {
      //   id: videos[index].id,
      //   title: videos[index].title,
      //   thumbnails: videos[index].thumbnails,
      //   duration_in_seconds: videos[index].duration_in_seconds
      // };
      $.ajax({
        type: "POST",
        url: "/party/" + party.getCode() + "/add",
        data: {
          id: video.getId(),
          title: video.getTitle()
        },
        success: function() {
          socket.emit('add', videoId);
        }
      });
    }
  });

  $checkboxPrimaryDevice.change(function(e) {
    primaryDevice = $checkboxPrimaryDevice.is(':checked');
    if(primaryDevice) {
      socket.emit('want-primary', party.getCode());
      $checkboxPrimaryDevice.data('requested', true);
    }
  });

  $checkboxPlayDevice.change(function(e) {
    playOnDevice = $(e.target).is(':checked');
  });

  $('#search').click(function(e) {
    e.preventDefault();
    $query = $('#query');

    if($query.val().length > 3) {
      $('#tube').tube({
        load: function() {
          $('#tube').empty();
        },
        click: function() {
          console.log('que?');
        },
        complete: function() {
          $tube.find('ol').addClass('list-group').find('li').addClass('list-group-item');
        },
        query: $query.val(),
        limit: 5,
        autoplay: false,
        events: { play: start, end: end, pause: pause },
        templates: {
          thumbnail: '<img src="{url}" title="{title}" class="img-rounded" />',
          title: '<h5 class="list-group-item-heading">{title}</h5>',
          statistics: '<span class="statistics"><span class="duration"><span class="glyphicon glyphicon-time"></span> {duration}</span><span class="views"><span class="glyphicon glyphicon-eye-open"></span> {views}</span></span>',
          video: [
            '<a href="#" data-video-id="{id}" data-index="{index}" class="add-item">',
              '<div class="col-md-3">{thumbnail}</div>',
              '<div class="col-md-9">{title}{statistics}</div>',
            '</a>'].join('')
        }
      });
    }
  });
});
